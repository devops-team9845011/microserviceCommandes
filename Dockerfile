FROM openjdk:8-jdk-alpine
ADD target/*.jar microservicecommandes.jar
ENTRYPOINT ["java","-jar","/microservicecommandes.jar"] 
